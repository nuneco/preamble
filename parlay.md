# We the undersigned wish to parlay.  
Come in peace and let us re-establish healthy communication.  

Parlay is a temporary peace to discuss how to fix communication breakdowns and cease hostilities, with a committment to establishing lasting peace and cooperation between individuals and groups.

All are welcome.  

Know that this is a place of law.  
There are expectations placed upon you if you choose to attend.  

----
## Predisposition for Peace and Communication
We will extend (and expect to be extended) a committment to the following basic communication minimums:  
* Facts matter.  
  * Learn to use [snopes.com](https://snopes.com).  
  * Learn about common [logical fallicies](https://yourlogicalfallacyis.com/) and intentionally avoid them, especially [the fallacy fallacy](https://yourlogicalfallacyis.com/the-fallacy-fallacy).  
* Challenges to fact will be decided on the spot, internet connectivity permitting.  
  * Anyone may ask for a fact check.  
  * Discussion of that topic pauses while facts are checked.  
  * Other non-blocked discussions can continue as normal, unless a full pause is requested.  
* Threats or threatening actions will result in a warning, and then expulsion.  
  * It's not a joke. Violence, violating consent, and harm to yourself or others are off limits for humor or action at parlay.  
  * If you can't restrain yourself for the duration of the parlay, bring us someone who can.  
* Language will be kept civil and generally profanity free.  
  * Curse like a pirate elsewhere.  
  * To the best of your ability, be specific, concise, and polite while communicating at parlay.  
* Personal attacks are not permitted.  
  * Claiming that it's a personal attack doesn't make it so.  
  * If the other person is being politely critical of faulty logic or unsound assumptions, they're doing you a favour.  
* Listen to others.  
  * Ensure all voices are heard by all.  
  * Call out the table for talking over others.  

----
## Proceedings
The parlay is led by a tribunal.  

The tribunal is filled by volunteers who have no voice in the proceedings.  
The tribunal exists to keep order and prevent toxicity from escalating.  

The tribunal has final say on matters of security.  
The tribunal must provide justification, in the form of daily log reports and meeting minutes, for their actions or lack of action.  
The tribunal answers to the [Audit Department](/auditors/about.md).  

### Specific Powers of the Tribunal
The tribunal is empowered to facilitate communication and peace.  

#### Gentle Mute
If a member of the tribunal wishes to speak, you must be silent.  
This is the first Specific Power of the tribunal: `Gentle Mute (absolute)`.  
This power facilitates the tribunal to raise otherwise-unheard voices to the forefront of conversation.

If you seek an advocate, speak with a member of the tribunal.
Your voice deserves to be heard.  

The tribunal will raise their voice alongside yours, and create space for you to speak.

#### Weapons on the Table
When faced with the reality of pirates, a member of the tribunal may ask to speak with you privately.  
You are permitted to bring your entourage, provided they bind themselves with this rule as you do.  
The tribunal will state "weapons on the table", at which time members of the parlay may, without consequences, divest themselves of weapon(s).

Any collected thus are stowed with the others, and everone goes back to whatever they were doing.

#### Expel, Ban, Permaban
If you have violated the rules, the tribunal can expel you.  
If you apologized, you may return after a short period of time, which ideally you'll spend in reflection of why you were kicked.  
If you choose to repeatedly violate the rules, you will be excluded from future proceedings.  

You can appeal a ban, but lifting a ban requires all three members of the tribunal to agree to reverse the decision.  

Good luck with that if you've been unpleasant to a lot of people.  

#### Break
If the proceedings are exhausting the participants, the tribunal may require a recess to be taken. The length of the recess is determined by a game of rock-paper-scissors-lizard-spock, the number of minutes determined by the amount of time it takes to complete the first full game involving all members of the proceedings.  

If the tribunal judges that the proceedings have not been sufficiently relaxed, any member of the tribunal may declare `calvinball`, which requires each member of the proceedings to add or modify one of the rules of the game, resulting in a list of `calvinball:houserules` being added to the official record of the proceedings.  

For the sanity of all involved, the tribunal is unable to require the completion of a second full game with aforementioned houserules in play, however the participants may opt to do so provided that everyone is willing to attempt it.  

#### Postpone Decision
Some decisions require additional thought. If the participants have reached a draft conclusion, the tribunal may require a (lengthy) pause and re-drafting, but not more than three times.

#### Archive
The tribunal will provide signed and encrypted copies of the proceedings to each group.  
A copy of all proceedings data will be filed with the [Archive](/archives/about.md).  
The proceedings will be open for three years, and then be closed and sent to long term storage.

----
### Opening Ceremonies
The tribunal declares the parlay to be open.  
All parties are formally welcomed to the proceedings.  
Each group is asked to send one representative to the table with the tribunal to exchange their instance, pod, or organization's public key(s) with the tribunal.  

#### Visual Organization
Participants are asked to identify themselves using a `green/blue/yellow/orange/red` badge, indicating their willingness to discuss events.  
 * Do not approach a red badge for discussion.  
 * Orange badges are self-identifying subject matter experts, who desire to exchange knowledge.  
 * Approach a yellow badge with caution, but expect them to be busy with other things.  
 * Be gentle with the blue badges, they need kindness today.
 * Green badges indicate a willingness to vigorously discuss the events in the light of hard logic and provable fact.  

> Obtain consent before debating or attempting to change someone's mind.  

#### Mandate for Meditation
After the exchange of encryption keys and the badging process, the [Captains](/captains/about.md) will lead guided meditations.  

Crews are encouraged to mingle without speaking.  
Noises above a low hum are discouraged, and vehicular transportation is suspended.  
Emergency services will continue to operate as normal.  

This practice is encouraged at minimum once per day.  
It is recommended that the day open and close with meditation sessions.  

#### Shared Meals
Meals decided by vote and donation.  
Cast a vote for an option, pay your portion of it into the pot.  
If you don't volunteer to cook it, don't complain how it tastes.  

Recipe sharing is encouraged.  
Crews are encouraged to dine with the tribunal.  

Everyone thanks the cooks.  
Everyone busses their own table.    

#### Curfew
The tribunal declares end of day.  

All conversations must come to a close.  
Silence is observed for the space of several minutes.  


Thank one another for listening.  

Go back to your places and nourish your bodies.  
Speak with friends.  
Be intentionally gentle and kind to one another.  
Acknowledge the events of the day, but do not discuss them.  


### @todo - finish this document
