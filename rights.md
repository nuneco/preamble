# Equality of Life

Life requires balance.
Life requires respect.
Life requires collaboration on equal terms.

All beings are equal in life.
No being has a larger, or smaller measure.

If a path is restricted, life will overcome.

If the balance is disrupted, restitution is necessary.
Each to their own ability, each to their own need.

No being can own another.
Not through violence, nor chains, nor through labor. 
A living being is inherently free.


## Explaining

A living being is something possessing life.
Mammals, plants, and bacteria all possess it, and each can take it from the others.
Death follows life, stalking birth across a myriad of years, taking the aged and unprepared, and leaving peace once the hunt is through.

Violence begets death, of the self and of other, and violence is an act of malice, thoughtlessness, or fear.

Consent is permission, and without it, one must act with caution and directness.